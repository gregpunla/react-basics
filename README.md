# React JS #

### What is React JS ###

React (JavaScript library) In computing, React (sometimes styled React.js or ReactJS) is a JavaScript library for building user interfaces. ... This corresponds to View in the Model-View-Controller (MVC) pattern, and can be used in combination with other JavaScript libraries or frameworks in MVC, such as AngularJS.

###Why React###
The main purpose of React is to be fast, scalable, and simple. It works only on user interfaces in application. This corresponds to view in the MVC template. It can be used with a combination of other JavaScript libraries or frameworks, such as Angular JS in MVC

### What is JSX ###
JSX stands for JavaScript XML. With React, it's an extension for XML-like code for elements and components. Per the React docs and as you mentioned: JSX is a XML-like syntax extension to ECMAScript without any defined semantics.

### Setting up React JS ###

* React Core JS
* Webpack
* Babel (es6 Transpiler)
* Node JS

### Steps ###

Npm init

Npm install react@15.2.1 react-dom@15.2.1 --save (Production Dependencies)

Npm install webpack webpack-cli webpack-dev-server babel-loader babel-core babel-preset-env babel-preset-react babel-preset-stage-2 --save-dev (Development Dependencies)

Package.json

```json
{
  "name": "react_basics",
  "version": "1.0.0",
  "description": "basic react",
  "main": "index.js",
  "scripts": {
    "start": "npm run build",
    "build": "webpack -d && cp src/index.html dist/index.html && webpack-dev-server --content-base src/ --inline --hot",
    "build:prod": "webpack -p && cp src/index.html dist/index.html"
  },
  "keywords": [
    "reactjs"
  ],
  "author": "Greg Punla",
  "license": "MIT",
  "dependencies": {
    "react": "^15.2.1",
    "react-dom": "^15.2.1",
    "webpack-cli": "^2.0.13"
  },
  "devDependencies": {
    "babel-loader": "^7.1.4",
    "babel-preset-env": "^1.6.1",
    "babel-preset-react": "^6.24.1",
    "babel-preset-stage-2": "^6.24.1",
    "webpack": "^4.2.0",
    "webpack-dev-server": "^3.1.1"
  }
}
```


Create Folder src > index.html > code basic html structure
```
<!DOCTYPE html>
<html>
<head>
	<title>React</title>
</head>
<body>
<script src="app/bundle.js"></script>
</body>
</html>
```

Create webpack.config.js > code webpack settings

```
var path = require("path");

var DIST_DIR = path.resolve(__dirname, "dist");
var SRC_DIR = path.resolve(__dirname, "src");

var config = {
    entry: SRC_DIR + "/app/index.js",
    output: {
        path: DIST_DIR + "/app",
        filename: "bundle.js",
        publicPath: "/app/"
    },
	 module: {
    rules: [
      {
        test: /\.js?/,
        include: SRC_DIR,
        loader: "babel-loader",
        query: {
          "presets": [
            "env",
            "react",
            "stage-2"
          ]
        }
      }
    ]
  }
};

module.exports = config;
```

Create app > index.js add

```
console.log("It Works!")
```

Add script to package json

```
  "scripts": {
    "start": "npm run build",
    "build": "webpack -d && cp src/index.html dist/index.html && webpack-dev-server --content-base src/ --inline --hot",
    "build:prod": "webpack -p && cp src/index.html dist/index.html"
  },
```


###React JS Components###

index.html

```
<!DOCTYPE html>
<html>
<head>
	<title>React</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div id="app"></div>
<script src="app/bundle.js"></script>
</body>
</html>
```

index.js

```
import React from "react";
import { render } from "react-dom";

class App extends React.Component {
	render() {
		return (
				<div className="container">
					<div className="row">
						<div className="col-xs-10 col-xs-offset-1">
							<h1>Hello!</h1>
						</div>
					</div>
				</div>
			);
	}
}

render(<App/>, window.document.getElementById("app"));
```

###Multiple Components###

Create app > components > Header.js

```
import React from "react";

export class Header extends React.Component {
	render() {
		return(
				<nav className="navbar navbar-default">
					<div className="container">
						<div className="navbar-header">
							<ul className="nav navbar-nav">
								<li>
									<a href="#">Home</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			);
	}
}
```

Create app > components > Home.js

```
import React from "react";

export class Home extends React.Component{
	render(){
		return (
			<div>
				<p>In a new component!</p>
			</div>
			);
	}
}
```

index.js 

```
import React from "react";
import { render } from "react-dom";

import { Header } from "./components/Header";
import { Home } from "./components/Home";

class App extends React.Component {
	render() {
		return (
				<div className="container">
					<div className="row">
						<div className="col-xs-10 col-xs-offset-1">
							<Header/>
						</div>
					</div>
					<div className="row">
						<div className="col-xs-10 col-xs-offset-1">
							<Home/>
						</div>
					</div>
				</div>
			);
	}
}

render(<App/>, window.document.getElementById("app"));
```

###Outputing Dynamic Data###

Home.js

```
import React from "react";

export class Home extends React.Component{
	render(){
		let content = "somecontent";
		if(content == "somecontent"){
			content = "newcontent";
		}
		return (
			<div>
				<p>In a new component!</p>
				<p>{content}</p>
				<p>{content == "newcontent" ? "Yes" : "No"}</p>
			</div>
			);
	}
}	
```

###Passing Data with Props###

Home.js

```
import React from "react";

export class Home extends React.Component{
	render(){
		console.log(this.props);
		let content = "somecontent";
		if(content == "somecontent"){
			content = "newcontent";
		}
		return (
			<div>
				<p>In a new component!</p>
				<p>{content}</p>
				<p>Your name is {this.props.name}, your age is {this.props.age}</p>
				<p>User object => Name: {this.props.user.name}</p>
				<div>
					<h4>Hobbies</h4>
						<ul>
							{this.props.user.hobbies.map((hobby, i) => <li key={i}>{hobby}</li>)}
						</ul>
				</div>
				<hr/>
				{this.props.children}
			</div>
			);
	}
}
```

Index.js

```
import React from "react";
import { render } from "react-dom";

import { Header } from "./components/Header";
import { Home } from "./components/Home";

class App extends React.Component {
	render() {
		var user = {
			name: "Anna",
			hobbies: ["Sports", "Reading"]
		};
		return (
				<div className="container">
					<div className="row">
						<div className="col-xs-10 col-xs-offset-1">
							<Header/>
						</div>
					</div>
					<div className="row">
						<div className="col-xs-10 col-xs-offset-1">
							<Home name={"Max"} age={27} user={user}>
								<p>This is inside home</p>
							</Home>
						</div>
					</div>
				</div>
			);
	}
}

render(<App/>, window.document.getElementById("app"));
```

###Evemts###


Home.js

```
import React from "react";

export class Home extends React.Component{
	constructor(props){
		super();
		this.age = props.age;
	}
	onMakeOlder(){
		this.age += 3;
		console.log(this.age);
	}
	render(){
		console.log(this.props);
		let content = "somecontent";
		if(content == "somecontent"){
			content = "newcontent";
		}
		return (
			<div>
				<p>In a new component!</p>
				<p>{content}</p>
				<p>Your name is {this.props.name}, your age is {this.props.age}</p>
				<p>User object => Name: {this.props.user.name}</p>
				<div>
					<h4>Hobbies</h4>
						<ul>
							{this.props.user.hobbies.map((hobby, i) => <li>{hobby}</li>)}
						</ul>
				</div>
				<hr/>
				{this.props.children}
				<button onClick={() => this.onMakeOlder()} className="btn btn-primary">Make me Older!</button>
			</div>
			);
	}
}
```

###State of Components###

```
import React from "react";

export class Home extends React.Component{
	constructor(props){
		super();
		this.state = {
			age: props.age,
		};
	}
	onMakeOlder(){
		this.setState({
			age: this.state.age + 3
		});
		console.log(this.age);
	}
	render(){
		console.log(this.props);
		let content = "somecontent";
		if(content == "somecontent"){
			content = "newcontent";
		}
		return (
			<div>
				<p>In a new component!</p>
				<p>{content}</p>
				<p>Your name is {this.props.name}, your age is {this.state.age}</p>
				<p>User object => Name: {this.props.user.name}</p>
				<div>
					<h4>Hobbies</h4>
						<ul>
							{this.props.user.hobbies.map((hobby, i) => <li>{hobby}</li>)}
						</ul>
				</div>
				<hr/>
				{this.props.children}
				<button onClick={() => this.onMakeOlder()} className="btn btn-primary">Make me Older!</button>
			</div>
			);
	}
}
```


