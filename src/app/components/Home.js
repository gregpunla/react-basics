import React from "react";

export class Home extends React.Component{
	constructor(props){
		super();
		this.state = {
			age: props.age,
		};
	}
	onMakeOlder(){
		this.setState({
			age: this.state.age + 3
		});
		console.log(this.age);
	}
	render(){
		console.log(this.props);
		let content = "somecontent";
		if(content == "somecontent"){
			content = "newcontent";
		}
		return (
			<div>
				<p>In a new component!</p>
				<p>{content}</p>
				<p>Your name is {this.props.name}, your age is {this.state.age}</p>
				<p>User object => Name: {this.props.user.name}</p>
				<div>
					<h4>Hobbies</h4>
						<ul>
							{this.props.user.hobbies.map((hobby, i) => <li>{hobby}</li>)}
						</ul>
				</div>
				<hr/>
				{this.props.children}
				<button onClick={() => this.onMakeOlder()} className="btn btn-primary">Make me Older!</button>
			</div>
			);
	}
}